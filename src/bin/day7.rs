// day7.rs
// bags containing other bags

#[macro_use]
extern crate lazy_static;

use advent_of_rust_2020::util;
use itertools::Itertools;
use regex::Regex;
use std::collections::{HashMap, HashSet};

fn main() {
    let input = util::read_all_stdin_to_lines();
    let rules: Vec<&str> = input.iter().map(|s| s.as_str()).collect();
    println!(
        "Part 1: outermost count {}",
        count_shiny_gold_outermost(&rules)
    );
    println!(
        "Part 2: bags in shiny gold {}",
        count_bags_in_shiny_gold(&rules)
    );
}

/// Parses colors from a container rule
/// "light red bags contain 1 bright white bag, 2 muted yellow bags."
/// becomes ("light red", vec!["bright white", "muted yellow"])
fn parse_rule(rule: &str) -> (&str, Vec<&str>) {
    lazy_static! {
        static ref RE_CONTAINEE_COLOR: Regex =
            Regex::new(r"\d (\D+?) bag").expect("failed to compile regex");
    }
    let (container, containee_rule) = rule
        .splitn(2, " bags contain ")
        .next_tuple()
        .expect("failed to parse rule");
    (
        container,
        RE_CONTAINEE_COLOR
            .captures_iter(containee_rule)
            .map(|caps| caps.get(1).unwrap().as_str())
            .collect(),
    )
}

/// maps from a bag color to the bag colors that can contain it
fn get_contained_by<'a>(rules: &[&'a str]) -> HashMap<&'a str, HashSet<&'a str>> {
    let mut contained_by = HashMap::new();
    for rule in rules {
        let (container, containees) = parse_rule(rule);
        for containee in containees {
            let containers = contained_by.entry(containee).or_insert_with(HashSet::new);
            containers.insert(container);
        }
    }
    contained_by
}

fn find_possible_outermost<'a>(
    contained_by: &HashMap<&str, HashSet<&'a str>>,
    containee: &str,
) -> HashSet<&'a str> {
    let empty = HashSet::new();
    let outers = contained_by.get(&containee).unwrap_or(&empty);
    let mut set = outers.iter().cloned().collect();
    for outer in outers {
        // Infinite recursion could happen if bag contain relationships form a loop, but then the
        // problem would be unsolvable, so we'll assume we don't have to check for that here
        set = &set | &find_possible_outermost(contained_by, outer);
    }
    set
}

fn count_shiny_gold_outermost(rules: &[&str]) -> usize {
    find_possible_outermost(&get_contained_by(rules), "shiny gold").len()
}

// part 2
/// Parses colors and counts from a container rule, part 2 version
/// "light red bags contain 1 bright white bag, 2 muted yellow bags."
/// becomes ("light red", vec![("bright white", 1), ("muted yellow", 2)])
fn parse_rule2(rule: &str) -> (&str, Vec<(&str, usize)>) {
    lazy_static! {
        static ref RE_CONTAINEE_COLOR_COUNT: Regex =
            Regex::new(r"(\d+) (\D+?) bag").expect("failed to compile regex");
    }
    let (container, containee_rule) = rule
        .splitn(2, " bags contain ")
        .next_tuple()
        .expect("failed to parse rule");
    (
        container,
        RE_CONTAINEE_COLOR_COUNT
            .captures_iter(containee_rule)
            .map(|caps| {
                (
                    caps.get(2).unwrap().as_str(),
                    caps.get(1)
                        .unwrap()
                        .as_str()
                        .parse()
                        .expect("unable to parse count as usize"),
                )
            })
            .collect(),
    )
}

/// maps from a bag color to the quantity of each bag color that it contains
fn get_contains<'a>(rules: &[&'a str]) -> HashMap<&'a str, Vec<(&'a str, usize)>> {
    rules.iter().map(|rule| parse_rule2(rule)).collect()
}

/// counts number of bags contained by a given bag, including itself
fn count_bags_contained_by(contains: &HashMap<&str, Vec<(&str, usize)>>, color: &str) -> usize {
    contains
        .get(color)
        .expect("couldn't find color")
        .iter()
        .map(|(color, count)| count * count_bags_contained_by(contains, color))
        .sum::<usize>()
        + 1
}

fn count_bags_in_shiny_gold(rules: &[&str]) -> usize {
    count_bags_contained_by(&get_contains(rules), "shiny gold") - 1
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 9] = [
        "light red bags contain 1 bright white bag, 2 muted yellow bags.",
        "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
        "bright white bags contain 1 shiny gold bag.",
        "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
        "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
        "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
        "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
        "faded blue bags contain no other bags.",
        "dotted black bags contain no other bags.",
    ];

    #[test]
    fn test_parse_rule() {
        assert_eq!(
            ("faded blue", vec![]),
            parse_rule("faded blue bags contain no other bags.")
        );
        assert_eq!(
            ("vibrant plum", vec!["faded blue", "dotted black"]),
            parse_rule("vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.")
        );
    }

    #[test]
    fn test_find_possible_outermost() {
        let containers = get_contained_by(&TEST_INPUT);
        let expected: HashSet<_> = ["bright white", "muted yellow", "dark orange", "light red"]
            .iter()
            .cloned()
            .collect();
        assert_eq!(expected, find_possible_outermost(&containers, "shiny gold"));
    }

    #[test]
    fn test_count_shiny_gold_outermost() {
        assert_eq!(4, count_shiny_gold_outermost(&TEST_INPUT));
    }

    // part 2
    const TEST_INPUT2: [&'static str; 7] = [
        "shiny gold bags contain 2 dark red bags.",
        "dark red bags contain 2 dark orange bags.",
        "dark orange bags contain 2 dark yellow bags.",
        "dark yellow bags contain 2 dark green bags.",
        "dark green bags contain 2 dark blue bags.",
        "dark blue bags contain 2 dark violet bags.",
        "dark violet bags contain no other bags.",
    ];

    #[test]
    fn test_parse_rule2() {
        assert_eq!(
            ("faded blue", vec![]),
            parse_rule2("faded blue bags contain no other bags.")
        );
        assert_eq!(
            ("vibrant plum", vec![("faded blue", 5), ("dotted black", 6)]),
            parse_rule2("vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.")
        );
    }

    #[test]
    fn test_get_contains() {
        let contains = get_contains(&TEST_INPUT);
        assert_eq!(
            vec![("faded blue", 5usize), ("dotted black", 6)],
            *contains.get("vibrant plum").unwrap()
        );
    }

    #[test]
    fn test_count_bags_in_shiny_gold() {
        assert_eq!(32, count_bags_in_shiny_gold(&TEST_INPUT));
        assert_eq!(126, count_bags_in_shiny_gold(&TEST_INPUT2));
    }
}
