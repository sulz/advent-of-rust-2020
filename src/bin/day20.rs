// day20.rs
// square jigsaw puzzle

#[macro_use]
extern crate lazy_static;

use advent_of_rust_2020::util;
use multimap::MultiMap;
use regex::Regex;
//use std::collections::HashSet;

fn main() {
    let input: Vec<String> = util::read_all_stdin_to_lines();
    let tiles = parse_input_to_tiles(&input.iter().map(|s| s.as_str()).collect::<Vec<&str>>());
    let corner_product = calc_corner_product(&tiles);
    println!("part 1 corner product: {}", corner_product);
}

// shape of an edge is 10 bits, stored in a u16
type Shape = u16;

#[derive(Debug, PartialEq)]
struct Tile {
    id: u32,
    sides: [Shape; 4],
}

fn parse_input_to_tiles(lines: &[&str]) -> Vec<Tile> {
    lines.chunks(12).map(|x| parse_tile(x)).collect()
}

fn parse_tile(lines: &[&str]) -> Tile {
    lazy_static! {
        static ref RE_TILE_ID: Regex =
            Regex::new(r"^Tile (\d+):").expect("failed to compile regex");
    }
    let id = RE_TILE_ID
        .captures(lines[0])
        .map(|caps|
                // safe to unwrap because the capture group is not optional
                caps.get(1)
                    .unwrap()
                    .as_str()
                    .parse::<u32>()
                    .expect("couldn't parse tile id"))
        .expect("couldn't parse line");

    // TODO: there is certainly a more succinct way to express this
    let top = lines[1]
        .chars()
        .map(|c| match c {
            '.' => 0,
            '#' => 1,
            _ => panic!("unexpected character {}", c),
        })
        .fold(0, |acc, bit| (acc << 1) + bit);
    let right = (0..10)
        .map(|i| {
            lines
                .get(1 + i)
                .expect("fewer lines than expected in tile")
                .chars()
                .nth(9)
                .expect("line was shorter than expected")
        })
        .map(|c| match c {
            '.' => 0,
            '#' => 1,
            _ => panic!("unexpected character {}", c),
        })
        .fold(0, |acc, bit| (acc << 1) + bit);
    let bottom = (0..10)
        .map(|i| {
            lines
                .get(10)
                .expect("fewer lines than expected in tile")
                .chars()
                .nth(9 - i)
                .expect("line was shorter than expected")
        })
        .map(|c| match c {
            '.' => 0,
            '#' => 1,
            _ => panic!("unexpected character {}", c),
        })
        .fold(0, |acc, bit| (acc << 1) + bit);
    let left = (0..10)
        .map(|i| {
            lines
                .get(10 - i)
                .expect("fewer lines than expected in tile")
                .chars()
                .next()
                .expect("empty line")
        })
        .map(|c| match c {
            '.' => 0,
            '#' => 1,
            _ => panic!("unexpected character {}", c),
        })
        .fold(0, |acc, bit| (acc << 1) + bit);

    Tile {
        id,
        sides: [top, right, bottom, left],
    }
}

/// create map from 10-bit puzzle tile edge shape to the ids of all puzzle tiles with that shape
fn create_edge_map(tiles: &[Tile]) -> MultiMap<Shape, u32> {
    let mut edge_map = MultiMap::new();
    for tile in tiles {
        for side in tile.sides.iter() {
            // puzzle tiles can be flipped over, so insert the side and its complement as possibilities
            edge_map.insert(*side, tile.id);
            edge_map.insert(complement(*side), tile.id);
        }
    }
    edge_map
}

/// find the complementary edge shape that matches a given edge shape
fn complement(side: Shape) -> Shape {
    // can't do this because BITS is unstable
    //side.reverse_bits() >> (Shape::BITS - 10)
    // only works because Shape is u16
    side.reverse_bits() >> 6
}

/// find number of edges that can be matched with other tiles
fn count_matching_edges(tile: &Tile, edge_map: &MultiMap<Shape, u32>) -> usize {
    tile.sides
        .iter()
        .filter(|side| {
            edge_map
                .get_vec(side)
                .unwrap()
                .iter()
                .any(|&tile_id| tile_id != tile.id)
        })
        .count()
}

/// Find the ids of the 4 corner tiles
///
/// TODO: Would the optimistic check be more efficiently done iterating through the edge map and
/// exploring the keys (shapes) with only one value (tile id)?
fn find_corner_tiles(tiles: &[Tile], edge_map: &MultiMap<Shape, u32>) -> Vec<u32> {
    // First we optimistically check for ids of puzzle tiles with exactly 2 sides with a match
    let definitely_corners: Vec<_> = tiles
        .iter()
        .filter(|tile| 2 == count_matching_edges(tile, &edge_map))
        .map(|tile| tile.id)
        .collect();
    if 4 == definitely_corners.len() {
        definitely_corners
    } else {
        // I don't think this path needs to be implemented because the outer edges don't match
        // anything
        unimplemented!();
    }
}

fn calc_corner_product(tiles: &[Tile]) -> u64 {
    let edge_map = create_edge_map(tiles);
    let corner_tile_ids = find_corner_tiles(&tiles, &edge_map);
    corner_tile_ids
        .iter()
        .fold(1, |product, &tile_id| product * (tile_id as u64))
}

// part 2
fn _int_sqrt(x: usize) -> Option<usize> {
    for root in 0.. {
        let square = root * root;
        if x == square {
            return Some(root);
        }
        if x < square {
            return None;
        }
    }

    unreachable!();
}

struct _PlacedTiles {
    tile_id: Vec<Option<u32>>,
    rotate_right: Vec<Option<u32>>,
    tiles_per_side: usize,
}

impl _PlacedTiles {
    fn _new(tiles_per_side: usize) -> _PlacedTiles {
        let tile_count = tiles_per_side * tiles_per_side;
        _PlacedTiles {
            tile_id: vec![None; tile_count],
            rotate_right: vec![None; tile_count],
            tiles_per_side,
        }
    }

    fn _place(&mut self, tile_id: u32, rotate_right: u32, x: usize, y: usize) {
        assert!(x < self.tiles_per_side);
        assert!(y < self.tiles_per_side);
        let offset = self.tiles_per_side * y + x;
        self.tile_id[offset] = Some(tile_id);
        self.rotate_right[offset] = Some(rotate_right);
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 107] = [
        "Tile 2311:",
        "..##.#..#.",
        "##..#.....",
        "#...##..#.",
        "####.#...#",
        "##.##.###.",
        "##...#.###",
        ".#.#.#..##",
        "..#....#..",
        "###...#.#.",
        "..###..###",
        "",
        "Tile 1951:",
        "#.##...##.",
        "#.####...#",
        ".....#..##",
        "#...######",
        ".##.#....#",
        ".###.#####",
        "###.##.##.",
        ".###....#.",
        "..#.#..#.#",
        "#...##.#..",
        "",
        "Tile 1171:",
        "####...##.",
        "#..##.#..#",
        "##.#..#.#.",
        ".###.####.",
        "..###.####",
        ".##....##.",
        ".#...####.",
        "#.##.####.",
        "####..#...",
        ".....##...",
        "",
        "Tile 1427:",
        "###.##.#..",
        ".#..#.##..",
        ".#.##.#..#",
        "#.#.#.##.#",
        "....#...##",
        "...##..##.",
        "...#.#####",
        ".#.####.#.",
        "..#..###.#",
        "..##.#..#.",
        "",
        "Tile 1489:",
        "##.#.#....",
        "..##...#..",
        ".##..##...",
        "..#...#...",
        "#####...#.",
        "#..#.#.#.#",
        "...#.#.#..",
        "##.#...##.",
        "..##.##.##",
        "###.##.#..",
        "",
        "Tile 2473:",
        "#....####.",
        "#..#.##...",
        "#.##..#...",
        "######.#.#",
        ".#...#.#.#",
        ".#########",
        ".###.#..#.",
        "########.#",
        "##...##.#.",
        "..###.#.#.",
        "",
        "Tile 2971:",
        "..#.#....#",
        "#...###...",
        "#.#.###...",
        "##.##..#..",
        ".#####..##",
        ".#..####.#",
        "#..#.#..#.",
        "..####.###",
        "..#.#.###.",
        "...#.#.#.#",
        "",
        "Tile 2729:",
        "...#.#.#.#",
        "####.#....",
        "..#.#.....",
        "....#..#.#",
        ".##..##.#.",
        ".#.####...",
        "####.#.#..",
        "##.####...",
        "##..#.##..",
        "#.##...##.",
        "",
        "Tile 3079:",
        "#.#.#####.",
        ".#..######",
        "..#.......",
        "######....",
        "####.#..#.",
        ".#...#.##.",
        "#.#####.##",
        "..#.###...",
        "..#.......",
        "..#.###...",
    ];

    #[test]
    fn test_parse_tile() {
        let expected = Tile {
            id: 2311,
            sides: [
                0b00_1101_0010,
                0b00_0101_1001,
                0b11_1001_1100,
                0b01_0011_1110,
            ],
        };
        assert_eq!(expected, parse_tile(&TEST_INPUT));
    }

    #[test]
    fn test_parse_input_to_tiles() {
        let tiles = parse_input_to_tiles(&TEST_INPUT);
        assert_eq!(9, tiles.len());
    }

    #[test]
    fn test_find_corner_tiles() {
        let tiles = parse_input_to_tiles(&TEST_INPUT);
        let edge_map = create_edge_map(&tiles);
        let mut corner_tiles = find_corner_tiles(&tiles, &edge_map);
        corner_tiles.sort();
        assert_eq!(vec![1171, 1951, 2971, 3079], corner_tiles);
    }

    #[test]
    fn test_calc_corner_product() {
        let tiles = parse_input_to_tiles(&TEST_INPUT);
        assert_eq!(20_899_048_083_289, calc_corner_product(&tiles));
    }
}
