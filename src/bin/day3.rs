// day3.rs
// navigate a forest

use advent_of_rust_2020::util;
use bitvec::prelude::*;

fn main() {
    let input = util::read_all_stdin_to_lines();
    let tree_strings: Vec<&str> = input.iter().map(|s| s as &str).collect();
    let trees = parse_input(&tree_strings).expect("Failed to parse trees");
    println!("Part 1: tree count {}", count_trees(&trees, 3, 1));
    println!("Part 2: tree product {}", tree_product(&trees));
}

#[derive(Debug, PartialEq)]
struct Forest {
    width: usize,
    positions: BitVec,
}

impl Forest {
    fn is_tree(&self, x: usize, y: usize) -> Option<bool> {
        let index = y * self.width + (x % self.width);
        self.positions.get(index).copied()
    }
}

fn parse_input(input: &[&str]) -> Option<Forest> {
    let width = input.first()?.len();
    // verify that all lines are the same length
    if !input.iter().all(|line| line.len() == width) {
        return None;
    }

    let positions: Option<BitVec> = input
        .iter()
        .flat_map(|s| {
            s.chars().map(|c| match c {
                '.' => Some(false),
                '#' => Some(true),
                _ => None,
            })
        })
        .collect();

    Some(Forest {
        width,
        positions: positions?,
    })
}

fn count_trees(trees: &Forest, right_count: usize, down_count: usize) -> usize {
    (1..)
        .map(|i| (right_count * i, down_count * i))
        .map(|(x, y)| trees.is_tree(x, y))
        .take_while(|x| x.is_some())
        .filter(|&x| x == Some(true))
        .count()
}

fn tree_product(trees: &Forest) -> usize {
    count_trees(&trees, 1, 1)
        * count_trees(&trees, 3, 1)
        * count_trees(&trees, 5, 1)
        * count_trees(&trees, 7, 1)
        * count_trees(&trees, 1, 2)
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 11] = [
        "..##.......",
        "#...#...#..",
        ".#....#..#.",
        "..#.#...#.#",
        ".#...##..#.",
        "..#.##.....",
        ".#.#.#....#",
        ".#........#",
        "#.##...#...",
        "#...##....#",
        ".#..#...#.#",
    ];

    #[test]
    fn test_parse_input() {
        #[rustfmt::skip]
        let expected_positions = bitvec![
            0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
            1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
            0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0,
            0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1,
            0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0,
            0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 
            0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1,
            0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0,
            1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1,
            0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1,
        ];
        assert_eq!(
            Some(Forest {
                width: 11,
                positions: expected_positions
            }),
            parse_input(&TEST_INPUT)
        );
        // invalid character is rejected
        assert_eq!(None, parse_input(&["..##", "..#🦄"]));
        // inconsistent line length is rejected
        assert_eq!(None, parse_input(&["..##", "..#"]));
    }

    #[test]
    fn test_is_tree() {
        let trees = parse_input(&TEST_INPUT).unwrap();
        assert!(!trees.is_tree(0, 0).unwrap());
        assert!(trees.is_tree(2, 0).unwrap());
        assert!(trees.is_tree(0, 1).unwrap());
        // test repeating trees
        assert!(trees.is_tree(14, 0).unwrap());
        // test going out of bounds
        assert_eq!(None, trees.is_tree(0, 12));
    }

    #[test]
    fn test_count_trees() {
        let trees = parse_input(&TEST_INPUT).unwrap();
        assert_eq!(2, count_trees(&trees, 1, 1));
        assert_eq!(7, count_trees(&trees, 3, 1));
        assert_eq!(3, count_trees(&trees, 5, 1));
        assert_eq!(4, count_trees(&trees, 7, 1));
        assert_eq!(2, count_trees(&trees, 1, 2));
    }

    #[test]
    fn test_tree_product() {
        let trees = parse_input(&TEST_INPUT).unwrap();
        assert_eq!(336, tree_product(&trees));
    }
}
