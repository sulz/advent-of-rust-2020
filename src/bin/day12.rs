// day12.rs
// navigation directions

use advent_of_rust_2020::util;

fn main() {
    let input: Vec<String> = util::read_all_stdin_to_lines();
    let actions: Vec<Action> = input.iter().map(|s| parse_action(&s)).collect();
    let final_pos = perform_actions(&actions);
    let dist = calc_manhattan_distance(&final_pos);
    println!("Part 1: manhattan distance = {}", dist);
    let final_pos2 = perform_waypoint_actions(&actions);
    let dist2 = calc_manhattan_distance(&final_pos2);
    println!("Part 2: manhattan distance = {}", dist2);
}

#[derive(PartialEq, Debug)]
enum Action {
    N(i32),
    S(i32),
    E(i32),
    W(i32),
    L(i32),
    R(i32),
    F(i32),
}

fn parse_action(s: &str) -> Action {
    let (action_str, amount_str) = s.split_at(1);
    let amount = amount_str.parse().expect("unable to parse amount");
    match action_str {
        "N" => Action::N(amount),
        "S" => Action::S(amount),
        "E" => Action::E(amount),
        "W" => Action::W(amount),
        "L" => Action::L(amount),
        "R" => Action::R(amount),
        "F" => Action::F(amount),
        _ => panic!("unable to parse action"),
    }
}

#[derive(PartialEq, Debug)]
struct Pos {
    x: i32,
    y: i32,
    dir: (i32, i32),
}

impl Pos {
    fn new() -> Pos {
        Pos {
            x: 0,
            y: 0,
            dir: (1, 0),
        }
    }
}

#[rustfmt::skip]
fn perform_action(pos: &Pos, action: &Action) -> Pos {
    let (x, y, dir) = (pos.x, pos.y, pos.dir);
    match action {
        Action::N(dist) => Pos { x, y: y + dist, dir },
        Action::S(dist) => Pos { x, y: y - dist, dir },
        Action::E(dist) => Pos { x: x + dist, y, dir },
        Action::W(dist) => Pos { x: x - dist, y, dir },
        Action::R(90) | Action::L(270) => Pos { x, y, dir: (dir.1, -dir.0) },
        Action::L(90) | Action::R(270) => Pos { x, y, dir: (-dir.1, dir.0) },
        Action::R(180) | Action::L(180) => Pos { x, y, dir: (-dir.0, -dir.1) },
        Action::F(dist) => Pos { x: x + dist * dir.0, y: y + dist * dir.1, dir },
        Action::R(_) | Action::L(_) => unimplemented!("only handle 90/180 degree turns"),
    }
}

fn perform_actions(actions: &[Action]) -> Pos {
    actions
        .iter()
        .fold(Pos::new(), |pos, action| perform_action(&pos, action))
}

fn calc_manhattan_distance(pos: &Pos) -> i32 {
    pos.x.abs() + pos.y.abs()
}

// part 2
fn get_initial_waypoint_pos() -> Pos {
    Pos {
        x: 0,
        y: 0,
        dir: (10, 1),
    }
}

#[rustfmt::skip]
fn perform_waypoint_action(pos: &Pos, action: &Action) -> Pos {
    let (x, y, dir) = (pos.x, pos.y, pos.dir);
    match action {
        Action::N(dist) => Pos { x, y, dir: (dir.0, dir.1 + dist) },
        Action::S(dist) => Pos { x, y, dir: (dir.0, dir.1 - dist) },
        Action::E(dist) => Pos { x, y, dir: (dir.0 + dist, dir.1) },
        Action::W(dist) => Pos { x, y, dir: (dir.0 - dist, dir.1) },
        Action::R(90) | Action::L(270) => Pos { x, y, dir: (dir.1, -dir.0) },
        Action::L(90) | Action::R(270) => Pos { x, y, dir: (-dir.1, dir.0) },
        Action::R(180) | Action::L(180) => Pos { x, y, dir: (-dir.0, -dir.1) },
        Action::F(dist) => Pos { x: x + dist * dir.0, y: y + dist * dir.1, dir },
        Action::R(_) | Action::L(_) => unimplemented!("only handle 90/180 degree turns"),
    }
}

fn perform_waypoint_actions(actions: &[Action]) -> Pos {
    actions
        .iter()
        .fold(get_initial_waypoint_pos(), |pos, action| {
            perform_waypoint_action(&pos, action)
        })
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 5] = ["F10", "N3", "F7", "R90", "F11"];
    const TEST_ACTIONS: [Action; 5] = [
        Action::F(10),
        Action::N(3),
        Action::F(7),
        Action::R(90),
        Action::F(11),
    ];

    #[test]
    fn test_parse_action() {
        let actions: Vec<_> = TEST_INPUT.iter().map(|s| parse_action(s)).collect();
        assert_eq!(&actions, &TEST_ACTIONS);
    }

    #[test]
    fn test_perform_actions() {
        let pos = perform_actions(&TEST_ACTIONS);
        assert_eq!(pos.x, 17);
        assert_eq!(pos.y, -8);
        assert_eq!(25, calc_manhattan_distance(&pos));
    }

    // part 2
    #[test]
    fn test_perform_waypoint_actions() {
        let pos = perform_waypoint_actions(&TEST_ACTIONS);
        assert_eq!(pos.x, 214);
        assert_eq!(pos.y, -72);
        assert_eq!(286, calc_manhattan_distance(&pos));
    }
}
