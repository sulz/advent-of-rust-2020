// day10.rs
// "joltage" differences

use advent_of_rust_2020::util;
use itertools::Itertools;

type Jolts = usize;

fn main() {
    let input: Vec<String> = util::read_all_stdin_to_lines();
    let adapters: Vec<Jolts> = input
        .iter()
        .map(|s| s.parse().expect("couldn't parse number"))
        .collect();
    let (_1j, _3j) = find_1j_3j_diffs(&adapters);
    println!("Part 1: result = {} x {} = {}", _1j, _3j, _1j * _3j);
    let arrangements = calc_total_arrangements(&adapters);
    println!("Part 2: total arrangements = {}", arrangements);
}

// count the differences in joltages, and return the number of 1j and 3j differences.
// There's an implied 3j difference between the highest adapter and the device.
// There's also an implied difference between the outlet (0) and the first adapter.
fn find_1j_3j_diffs(adapters: &[Jolts]) -> (usize, usize) {
    // Chain in a 0 value for the initial connection to the outlet
    [0].iter()
        .chain(adapters.iter())
        // sort
        .sorted()
        // count differences of 1 jolt and 3 jolts
        .tuple_windows()
        .fold((0, 1), |(count_1j, count_3j), (&prev, &cur)| {
            match cur - prev {
                1 => (count_1j + 1, count_3j),
                3 => (count_1j, count_3j + 1),
                _ => (count_1j, count_3j),
            }
        })
}

// part 2

// A run of N 1-diffs leads to how many ways to combine that chain of adapters?
// run of 1 1-diff (ex: 4, 5, 8): 1
// run of 2 1-diffs (ex: 4, 5, 6, 9): 2
// run of 3 1-diffs (ex: 4, 5, 6, 7, 10): 4
// run of 4 1-diffs (ex: 4, 5, 6, 7, 8, 11): 7
// The sequence continues: 1, 2, 4, 7, 13, 24
// This looks a lot like tribonacci numbers. I haven't proven it mathematically, I'll
// just guess that it's right.
// https://oeis.org/A000073
fn get_tribonnaci_number(x: usize) -> usize {
    match x {
        0 => 1,
        1 => 1,
        2 => 2,
        _ => {
            get_tribonnaci_number(x - 1)
                + get_tribonnaci_number(x - 2)
                + get_tribonnaci_number(x - 3)
        }
    }
}

fn calc_total_arrangements(adapters: &[Jolts]) -> usize {
    // Final connection to the device makes no difference in the number of arrangements
    // But initial connection to the outlet does matter, so chain in a 0 value for it
    let diffs: Vec<usize> = [0]
        .iter()
        .chain(adapters.iter())
        // sort the adapters
        .sorted()
        // diff them
        .tuple_windows()
        .map(|(x, y)| y - x)
        .collect();

    // This assumes that the only differences between joltages are 1 and 3. Won't work if there
    // are any differences of 2 in there.
    assert!(diffs.iter().all(|&x| x == 1 || x == 3));
    // I'm also taking a shortcut because there always seems to be a 1 jolt adapter
    assert!(*diffs.first().unwrap() == 1);

    let (last_run, arrangements) = diffs.iter().tuple_windows().fold(
        (1, 1),
        |(current_run, arrangements), (&prev_diff, &curr_diff)| {
            if prev_diff == 1 && curr_diff != 1 {
                // end of run
                (0, arrangements * get_tribonnaci_number(current_run))
            } else if curr_diff == 1 {
                // in a run
                (current_run + 1, arrangements)
            } else {
                // not in a run
                (0, arrangements)
            }
        },
    );

    arrangements * get_tribonnaci_number(last_run)
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT1: [Jolts; 11] = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];
    const TEST_INPUT2: [Jolts; 31] = [
        28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8,
        17, 7, 9, 4, 2, 34, 10, 3,
    ];

    #[test]
    fn test_find_1j_3j_diffs() {
        assert_eq!((7, 5), find_1j_3j_diffs(&TEST_INPUT1));
        assert_eq!((22, 10), find_1j_3j_diffs(&TEST_INPUT2));
    }

    // part 2
    #[test]
    fn test_calc_tribonacci_number() {
        assert_eq!(121415, get_tribonnaci_number(20));
    }

    #[test]
    fn test_calc_total_arrangements() {
        assert_eq!(8, calc_total_arrangements(&TEST_INPUT1));
        assert_eq!(19208, calc_total_arrangements(&TEST_INPUT2));
    }
}
