// day13.rs
// adjacent multiples

use advent_of_rust_2020::util;
use itertools::Itertools;
use num::integer::lcm;

fn main() {
    let input: Vec<String> = util::read_all_stdin_to_lines();
    let timestamp: u32 = input[0].parse().expect("unable to parse timestamp");
    let bus_ids = parse_bus_ids(&input[1]);
    let result = find_earliest_leave_time(timestamp, &bus_ids);
    println!("Part 1: bus id x wait minutes = {} ", result);
    let buses = parse_bus_offset_ids(&input[1]);
    let depart_time = find_contest_depart_time(&buses);
    println!("Part 2: contest depart time = {} ", depart_time);
}

// "7,13,x,x,59,x,31,19" -> vec![7, 13, 59, 31, 19]
fn parse_bus_ids(s: &str) -> Vec<u32> {
    s.split(",").filter_map(|item| item.parse().ok()).collect()
}

// find earliest time you can leave * number of minutes you'll wait
fn find_earliest_leave_time(start_time: u32, bus_ids: &[u32]) -> u32 {
    for t in start_time.. {
        if let Some(bus_id) = bus_ids.iter().find(|&bus_id| t % bus_id == 0) {
            return bus_id * (t - start_time);
        }
    }
    unreachable!();
}

// part 2
// for part 2, we need both the bus id and its offset in the input list
// "7,13,x,x,59,x,31,19" -> vec![(4, 59), (6, 31), (7, 19), (1, 13), (0, 7)]
fn parse_bus_offset_ids(s: &str) -> Vec<(u64, u64)> {
    s.split(",")
        // "7" => Some(7), "x" => None
        .map(|item| item.parse().ok())
        // add index
        .enumerate()
        // filter out the "x" items, preserving the index
        .filter_map(|(i, bus_id)| {
            if let Some(bus_id) = bus_id {
                Some((i as u64, bus_id))
            } else {
                None
            }
        })
        // sort from greatest bus ID to smallest, gets us to the answer sooner
        .sorted_by(|(_, a), (_, b)| Ord::cmp(&b, &a))
        .collect()
}

// to find an aocomposite number:
// start at a given point, increment by a given increment
// at each point in your numeric journey, add the offset and check if 'factor' is a factor
// if so, your current point is the new aocomposite so return it!
fn find_aocomposite(start: u64, incr: u64, (offset, factor): (u64, u64)) -> u64 {
    (1..)
        .map(|i| i * incr + start)
        .find(|t| (t + offset) % factor == 0)
        .unwrap()
}

// Contest depart time is the aocomposite of every bus.
// Bus id is the factor, and position in the list is the offset.
// To make things faster, the increment is the least common multiple of all the bus ids we've seen so far.
fn find_contest_depart_time(buses: &[(u64, u64)]) -> u64 {
    buses
        .iter()
        .fold((1, 1), |(start, incr), bus| {
            (find_aocomposite(start, incr, *bus), lcm(incr, bus.1))
        })
        .0
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 2] = ["939", "7,13,x,x,59,x,31,19"];

    #[test]
    fn test_parse_bus_ids() {
        assert_eq!(parse_bus_ids(TEST_INPUT[1]), &[7, 13, 59, 31, 19]);
    }

    #[test]
    fn test_find_earliest_leave_time() {
        let bus_ids = parse_bus_ids(&TEST_INPUT[1]);
        assert_eq!(find_earliest_leave_time(939, &bus_ids), 295);
    }

    // part 2
    #[test]
    fn test_parse_bus_offset_ids() {
        assert_eq!(
            parse_bus_offset_ids(TEST_INPUT[1]),
            &[(4, 59), (6, 31), (7, 19), (1, 13), (0, 7)]
        );
    }

    #[test]
    fn test_find_contest_depart_time() {
        let bus_ids = parse_bus_offset_ids(&TEST_INPUT[1]);
        assert_eq!(find_contest_depart_time(&bus_ids), 1068781);
        assert_eq!(
            3417,
            find_contest_depart_time(&parse_bus_offset_ids("17,x,13,19"))
        );
        assert_eq!(
            754_018,
            find_contest_depart_time(&parse_bus_offset_ids("67,7,59,61"))
        );
        assert_eq!(
            779_210,
            find_contest_depart_time(&parse_bus_offset_ids("67,x,7,59,61"))
        );
        assert_eq!(
            1_261_476,
            find_contest_depart_time(&parse_bus_offset_ids("67,7,x,59,61"))
        );
        assert_eq!(
            1_202_161_486,
            find_contest_depart_time(&parse_bus_offset_ids("1789,37,47,1889"))
        );
    }
}
