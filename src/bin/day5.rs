// day5.rs
// binary conversion, boarding seat

use advent_of_rust_2020::util;

fn main() {
    let input: Vec<String> = util::read_all_stdin_to_lines();
    let encoded_seats: Vec<&str> = input.iter().map(|s| s.as_str()).collect();
    let highest = highest_seat_id(&encoded_seats);
    println!("Part 1: highest seat id {}", highest);
    let seat_id = find_seat_id(&encoded_seats);
    println!("Part 2: seat id {}", seat_id);
}

fn decode_seat_number(seat: &str) -> u16 {
    seat.chars().fold(0, |acc, c| {
        if c == 'B' || c == 'R' {
            acc * 2 + 1
        } else {
            acc * 2
        }
    })
}

fn highest_seat_id(seats: &[&str]) -> u16 {
    seats
        .iter()
        .map(|seat| decode_seat_number(seat))
        .max()
        .expect("Didn't find any seats")
}

// part 2
fn find_seat_id(seats: &[&str]) -> u16 {
    // find unused seat id where id + 1 and id - 1 exist
    // We use a vector instead of something like a BinaryHeap so we can use windows()
    let mut sorted_seats: Vec<_> = seats.iter().map(|seat| decode_seat_number(seat)).collect();
    sorted_seats.sort_unstable();
    sorted_seats
        .as_slice()
        .windows(2)
        .find(|&x| x[0] == x[1] - 2)
        .map(|x| x[0] + 1)
        .expect("No free seat exists")
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_decode_seat_number() {
        assert_eq!(357, decode_seat_number("FBFBBFFRLR"));
        assert_eq!(567, decode_seat_number("BFFFBBFRRR"));
        assert_eq!(119, decode_seat_number("FFFBBBFRRR"));
        assert_eq!(820, decode_seat_number("BBFFBBFRLL"));
    }

    #[test]
    fn test_highest_seat_id() {
        let input = ["FBFBBFFRLR", "BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"];

        assert_eq!(820, highest_seat_id(&input));
    }

    #[test]
    fn test_find_seat_id() {
        let input = [
            "FBFBBFFRLR",
            "FBFBBFFRRL",
            //"FBFBBFFRRR",
            "FBFBBFRLLL",
            "FBFBBFRLLR",
        ];

        assert_eq!(359, find_seat_id(&input));
    }
}
