// day2.rs
// valid passwords

use advent_of_rust_2020::util;

fn main() {
    let input = util::read_all_stdin_to_lines();
    let valid_count = input.iter().filter(|p| is_password_valid(&p)).count();
    println!("part 1: valid passwords {}", valid_count);
    let valid_count2 = input.iter().filter(|p| is_password_valid2(&p)).count();
    println!("part 2: valid passwords {}", valid_count2);
}

fn is_password_valid(password_field: &str) -> bool {
    let password_policy = parse_password_policy(&password_field).expect("unable to parse password");
    let count = password_policy
        .password
        .chars()
        .filter(|c| *c == password_policy.character)
        .count();
    count <= password_policy.max && count >= password_policy.min
}

#[derive(Debug, PartialEq)]
struct PasswordPolicy<'a> {
    min: usize,
    max: usize,
    character: char,
    password: &'a str,
}

fn parse_password_policy(policy_str: &str) -> Option<PasswordPolicy> {
    // example input: "1-3 a: abcde"
    let mut parts = policy_str.split(|c| c == '-' || c == ' ' || c == ':');
    Some(PasswordPolicy {
        min: parts.next()?.parse().ok()?,
        max: parts.next()?.parse().ok()?,
        character: parts.next()?.parse().ok()?,
        password: parts.nth(1)?,
    })
}

// part 2
fn is_password_valid2(password_field: &str) -> bool {
    let password_policy = parse_password_policy(&password_field).expect("unable to parse password");
    let mut password_chars = password_policy
        .password
        .chars()
        .skip(password_policy.min - 1);
    let does_first_match = password_chars.next().unwrap_or('🦄') == password_policy.character;
    let does_second_match = password_chars
        .nth(password_policy.max - password_policy.min - 1)
        .unwrap_or('🦄')
        == password_policy.character;
    does_first_match != does_second_match
}

#[test]
fn test_is_password_valid() {
    assert!(is_password_valid("1-3 a: abcde"));
    assert!(!is_password_valid("1-3 b: cdefg"));
    assert!(is_password_valid("2-9 c: ccccccccc"));
}

#[test]
fn test_parse_password_policy() {
    assert_eq!(
        Some(PasswordPolicy {
            min: 1,
            max: 3,
            character: 'a',
            password: "abcde"
        }),
        parse_password_policy("1-3 a: abcde")
    );
}

#[test]
fn test_is_password_valid2() {
    assert!(is_password_valid2("1-3 a: abcde"));
    assert!(!is_password_valid2("1-3 b: cdefg"));
    assert!(!is_password_valid2("2-9 c: ccccccccc"));
}
