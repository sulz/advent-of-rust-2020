// day1.rs

use itertools::Itertools;
use std::io::BufRead;

fn main() {
    let input: Vec<u32> = std::io::stdin()
        .lock()
        .lines()
        .map(|l| {
            l.expect("Failed to read line")
                .parse()
                .expect("Failed to parse line")
        })
        .collect();
    println!("part 1: {}", find_2020_product(&input, 2).unwrap());
    println!("part 2: {}", find_2020_product(&input, 3).unwrap());
}

fn find_2020_product(v: &[u32], count: usize) -> Option<u32> {
    for pair in v.iter().combinations(count) {
        if pair.iter().copied().sum::<u32>() == 2020 {
            return Some(pair.iter().copied().product());
        }
    }
    None
}

#[test]
fn test_find_2020_product() {
    let v = [1721, 979, 366, 299, 675, 1456];
    assert_eq!(Some(514579), find_2020_product(&v, 2));
    let v2 = [1010, 1721, 299];
    assert_eq!(Some(514579), find_2020_product(&v2, 2));
}

#[test]
fn test_find_2020_product3() {
    let v = [1721, 979, 366, 299, 675, 1456];
    assert_eq!(Some(241861950), find_2020_product(&v, 3));
    let v2 = [827, 979, 366, 675];
    assert_eq!(Some(241861950), find_2020_product(&v2, 3));
}
