// day6.rs
// unique answers

use advent_of_rust_2020::util;
use std::collections::HashSet;

fn main() {
    let input = util::read_all_stdin();
    println!(
        "Part 1: total unique_yes questions {}",
        total_unique_yes_questions(&input)
    );
    println!(
        "Part 2: total all yes questions {}",
        total_all_yes_questions(&input)
    );
}

fn count_unique_yes_questions(group: &str) -> usize {
    let unique_answers: HashSet<_> = group.chars().filter(|c| !c.is_whitespace()).collect();
    unique_answers.len()
}

fn total_unique_yes_questions(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|s| count_unique_yes_questions(s))
        .sum()
}

// part 2

fn count_all_yes_questions(group: &str) -> usize {
    let sets: Vec<HashSet<char>> = group
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().collect::<HashSet<_>>())
        .collect();
    // union to find all yes answers anyone gave
    let all_questions = sets.iter().fold(HashSet::new(), |acc, set| &acc | set);
    // intersection to find only answers everyone gave
    sets.iter().fold(all_questions, |acc, set| &acc & set).len()
}

fn total_all_yes_questions(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|s| count_all_yes_questions(s))
        .sum()
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: &'static str = "abc

a
b
c

ab
ac

a
a
a
a

b";

    #[test]
    fn test_count_unique_yes_questions() {
        // abcxyz
        assert_eq!(6, count_unique_yes_questions("abcx\nabcy\nabcz"));
    }

    #[test]
    fn test_total_unique_yes_questions() {
        assert_eq!(11, total_unique_yes_questions(TEST_INPUT));
    }

    // part 2
    #[test]
    fn test_count_all_yes_questions() {
        // a
        assert_eq!(1, count_all_yes_questions("ab\nac"));
    }

    #[test]
    fn test_total_all_yes_questions() {
        assert_eq!(6, total_all_yes_questions(TEST_INPUT));
    }
}
