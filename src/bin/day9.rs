// day9.rs
// sliding window sums

use advent_of_rust_2020::util;
use itertools::Itertools;

fn main() {
    let input = util::read_all_stdin_to_lines();
    let numbers: Vec<u64> = input
        .iter()
        .map(|s| {
            println!("{}", s);
            s.parse().expect("couldn't parse number")
        })
        .collect();
    let weak_number = find_weak_number(&numbers, 25);
    println!("Part 1: first weak number = {}", weak_number);
    let (smallest, largest) = find_weak_range(&numbers, weak_number).expect("no range found!");
    println!(
        "Part 2: weak range sum: {}+{} = {}",
        smallest,
        largest,
        smallest + largest
    );
}

// Naive slow implementation that scans the whole preamble every time
// If this is fast enough, I won't bother with something cleverer
fn is_weak_number(number: u64, preamble: &[u64]) -> bool {
    preamble
        .iter()
        .combinations(2)
        .find(|pair| pair[0] + pair[1] == number)
        .is_none()
}

// the weak number is the first number that is not the sum of 2 of the numbers in its preamble
fn find_weak_number(numbers: &[u64], preamble_size: usize) -> u64 {
    numbers
        .windows(preamble_size + 1)
        .find(|w| is_weak_number(w[preamble_size], &w[0..preamble_size]))
        .expect("no weak number found")[preamble_size]
}

// part 2
// again, going the naive route unless it proves too slow
fn find_weak_range(numbers: &[u64], weak_number: u64) -> Option<(u64, u64)> {
    for start_idx in 0..numbers.len() {
        let mut sum = 0;
        for (end_idx, number) in numbers[start_idx..].iter().enumerate() {
            sum += *number;
            if sum == weak_number {
                let range = &numbers[start_idx..=(start_idx + end_idx)];
                return Some((*range.iter().min().unwrap(), *range.iter().max().unwrap()));
            }
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [u64; 20] = [
        35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576,
    ];

    #[test]
    fn test_find_weak_number() {
        assert_eq!(127, find_weak_number(&TEST_INPUT, 5));
    }

    #[test]
    fn test_find_weak_range() {
        assert_eq!(Some((15, 47)), find_weak_range(&TEST_INPUT, 127));
    }
}
