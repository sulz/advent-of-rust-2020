// day24.rs
// hex grid

use advent_of_rust_2020::util;
use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::Add;

fn main() {
    let tiles: Vec<Pos> = util::parse_all_stdin_lines(|s| parse_pos(&s));
    let black_count = count_black_tiles(&tiles);
    println!("part 1 black tiles: {}", black_count);
    let black_count_100 = count_black_100_days(&tiles);
    println!("part 2 black tiles: {}", black_count_100);
}

// We use (e, nw) coordinate system convention
// So movement in the hex grid works like this:
// e: (+1 e, 0 nw)
// se: (0 e, -1 nw)
// sw: (-1 e, -1 nw)
// w: (-1 e, 0 nw)
// nw: (0 e, +1 nw)
// ne: (+1 e, +1 nw)

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Pos {
    e: i64,
    nw: i64,
}

impl Add for Pos {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {
            e: self.e + other.e,
            nw: self.nw + other.nw,
        }
    }
}

const E: Pos = Pos { e: 1, nw: 0 };
const SE: Pos = Pos { e: 0, nw: -1 };
const SW: Pos = Pos { e: -1, nw: -1 };
const W: Pos = Pos { e: -1, nw: 0 };
const NW: Pos = Pos { e: 0, nw: 1 };
const NE: Pos = Pos { e: 1, nw: 1 };

// parse the next direction from the input string
// return the updated position and subslice of the string
fn parse_dir(s: &str, pos: Pos) -> (&str, Pos) {
    match s.chars().next() {
        Some('e') => (&s[1..], pos + E),
        Some('w') => (&s[1..], pos + W),
        Some('s') => match s.chars().nth(1) {
            Some('e') => (&s[2..], pos + SE),
            Some('w') => (&s[2..], pos + SW),
            _ => panic!("unexpected character or end of line"),
        },
        Some('n') => match s.chars().nth(1) {
            Some('e') => (&s[2..], pos + NE),
            Some('w') => (&s[2..], pos + NW),
            _ => panic!("unexpected character or end of line"),
        },
        _ => panic!("unexpected character or end of line"),
    }
}

// parse a line of input, find the corresponding position in the hex grid
fn parse_pos(s: &str) -> Pos {
    let mut string_pos = (s, Pos { e: 0, nw: 0 });
    while !string_pos.0.is_empty() {
        string_pos = parse_dir(string_pos.0, string_pos.1)
    }
    string_pos.1
}

fn count_black_tiles(tiles: &[Pos]) -> usize {
    let mut is_black = HashMap::new();

    for tile in tiles {
        let x = is_black.entry(tile).or_insert(false);
        *x = !*x;
    }

    is_black.iter().filter(|(_, &val)| val).count()
}

// Part 2
type BlackTileSet = HashSet<Pos>;
fn get_black_tile_set(tiles: &[Pos]) -> BlackTileSet {
    let mut is_black = HashMap::new();

    for tile in tiles {
        let x = is_black.entry(tile).or_insert(false);
        *x = !*x;
    }

    is_black
        .iter()
        .filter(|(_, &val)| val)
        .map(|(&&pos, _)| pos)
        .collect()
}

fn count_adjacent_black_tiles(pos: &Pos, tiles: &BlackTileSet) -> usize {
    [E, SE, SW, W, NW, NE]
        .iter()
        .filter(|&&dir| tiles.contains(&(*pos + dir)))
        .count()
}

fn get_adjacent_white_flip_tiles(pos: &Pos, tiles: &BlackTileSet) -> Vec<Pos> {
    [E, SE, SW, W, NW, NE]
        .iter()
        // map to adjacent position
        .map(|&dir| *pos + dir)
        // keep only white tiles
        .filter(|adjacent_pos| !tiles.contains(adjacent_pos))
        // keep only tiles with exactly 2 black neighbors
        .filter(|white_pos| count_adjacent_black_tiles(white_pos, tiles) == 2)
        .collect()
}

// black tile: 0 black adjacent or > 2 black adjacent flipped to white
// white tile with exactly 2 black tiles adjacent flipped to black
//
// Put another way, black tiles are:
// black and 1 or 2 black adjacent
// empty/white and 2 black adjacent
//
// So if we have a set of current black positions, algo for new set is:
// - get a list of adjacent black tile directions
// - if list is length 1 or 2: we're in the new set
// - check every adjacent white tile, if it has 2 black neighbors it is also in the set
fn flip_tiles(tiles: &BlackTileSet) -> BlackTileSet {
    // this is inefficient memory allocation behavior, creating a new HashSet each time
    // only fix it if we have performance issues
    let mut new_tiles = HashSet::with_capacity(tiles.len());
    for pos in tiles {
        let black_adj_count = count_adjacent_black_tiles(pos, tiles);
        if black_adj_count == 2 || black_adj_count == 1 {
            new_tiles.insert(*pos);
        }
        let flip_tiles = get_adjacent_white_flip_tiles(pos, tiles);
        for flip_pos in flip_tiles {
            new_tiles.insert(flip_pos);
        }
    }
    new_tiles
}

// flip tiles n times
fn flip_tiles_n(tiles: &BlackTileSet, count: u32) -> BlackTileSet {
    if count == 0 {
        return tiles.clone();
    }
    let mut new_tiles = flip_tiles(tiles);
    for _ in 1..count {
        new_tiles = flip_tiles(&new_tiles);
    }
    new_tiles
}

fn count_black_100_days(tiles: &[Pos]) -> usize {
    let black_tiles = get_black_tile_set(&tiles);
    flip_tiles_n(&black_tiles, 100).len()
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 20] = [
        "sesenwnenenewseeswwswswwnenewsewsw",
        "neeenesenwnwwswnenewnwwsewnenwseswesw",
        "seswneswswsenwwnwse",
        "nwnwneseeswswnenewneswwnewseswneseene",
        "swweswneswnenwsewnwneneseenw",
        "eesenwseswswnenwswnwnwsewwnwsene",
        "sewnenenenesenwsewnenwwwse",
        "wenwwweseeeweswwwnwwe",
        "wsweesenenewnwwnwsenewsenwwsesesenwne",
        "neeswseenwwswnwswswnw",
        "nenwswwsewswnenenewsenwsenwnesesenew",
        "enewnwewneswsewnwswenweswnenwsenwsw",
        "sweneswneswneneenwnewenewwneswswnese",
        "swwesenesewenwneswnwwneseswwne",
        "enesenwswwswneneswsenwnewswseenwsese",
        "wnwnesenesenenwwnenwsewesewsesesew",
        "nenewswnwewswnenesenwnesewesw",
        "eneswnwswnwsenenwnwnwwseeswneewsenese",
        "neswnwewnwnwseenwseesewsenwsweewe",
        "wseweeenwnesenwwwswnew",
    ];

    #[test]
    fn test_parse_dir() {
        assert_eq!(("wswee", NW), parse_dir("nwwswee", Pos { e: 0, nw: 0 }));
    }

    #[test]
    fn test_parse_pos() {
        assert_eq!(Pos { e: 0, nw: -1 }, parse_pos("esew"));
        assert_eq!(Pos { e: 0, nw: 0 }, parse_pos("nwwswee"));
        assert_eq!(Pos { e: 3, nw: 0 }, parse_pos("esenee"));
    }

    #[test]
    fn test_count_black_tiles() {
        let tiles: Vec<Pos> = TEST_INPUT.iter().map(|line| parse_pos(&line)).collect();
        assert_eq!(10, count_black_tiles(&tiles));
    }

    // part 2
    #[test]
    fn test_get_black_tile_map() {
        let tiles: Vec<Pos> = TEST_INPUT.iter().map(|line| parse_pos(&line)).collect();
        let black_tiles = get_black_tile_set(&tiles);
        assert_eq!(10, black_tiles.len());
    }

    #[test]
    fn test_flip_tiles() {
        let mut tiles = get_black_tile_set(
            &TEST_INPUT
                .iter()
                .map(|line| parse_pos(&line))
                .collect::<Vec<Pos>>(),
        );
        // day 1
        tiles = flip_tiles(&tiles);
        assert_eq!(15, tiles.len());
        // day 2
        tiles = flip_tiles(&tiles);
        assert_eq!(12, tiles.len());
        // day 3
        tiles = flip_tiles(&tiles);
        assert_eq!(25, tiles.len());
        // day 4
        tiles = flip_tiles(&tiles);
        assert_eq!(14, tiles.len());
        // day 5
        tiles = flip_tiles(&tiles);
        assert_eq!(23, tiles.len());
        // day 6
        tiles = flip_tiles(&tiles);
        assert_eq!(28, tiles.len());
        // day 7
        tiles = flip_tiles(&tiles);
        assert_eq!(41, tiles.len());
        // day 8
        tiles = flip_tiles(&tiles);
        assert_eq!(37, tiles.len());
        // day 9
        tiles = flip_tiles(&tiles);
        assert_eq!(49, tiles.len());
        // day 10
        tiles = flip_tiles(&tiles);
        assert_eq!(37, tiles.len());
    }

    // test takes over a second to run in debug, skip by default
    #[test] #[ignore]
    fn test_flip_tiles_n() {
        let mut tiles = get_black_tile_set(
            &TEST_INPUT
                .iter()
                .map(|line| parse_pos(&line))
                .collect::<Vec<Pos>>(),
        );
        // day 20
        tiles = flip_tiles_n(&tiles, 20);
        assert_eq!(132, tiles.len());
        // day 30
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(259, tiles.len());
        // day 40
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(406, tiles.len());
        // day 50
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(566, tiles.len());
        // day 60
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(788, tiles.len());
        // day 70
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(1106, tiles.len());
        // day 80
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(1373, tiles.len());
        // day 90
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(1844, tiles.len());
        // day 100
        tiles = flip_tiles_n(&tiles, 10);
        assert_eq!(2208, tiles.len());
    }
}
