// day8.rs
// machine instruction interpreter

#[macro_use]
extern crate lazy_static;

use advent_of_rust_2020::util;
use regex::Regex;
use std::collections::HashSet;

fn main() {
    let input = util::read_all_stdin_to_lines();
    let instructions_input: Vec<&str> = input.iter().map(|s| s.as_str()).collect();
    let acc = find_acc_at_loop(&instructions_input);
    println!("Part 1: acc={}", acc);
    let acc =
        find_acc_at_termination(&instructions_input).expect("found no terminating condition!");
    println!("Part 2: acc={}", acc);
}

// I've arbitrarily decided that the accumulator and program counter are both 32 bits
#[derive(PartialEq, Debug)]
enum Instruction {
    Acc(i32),
    Jmp(i32),
    Nop(i32),
}

#[derive(PartialEq, Debug)]
struct ProgramState {
    pc: u32,
    acc: i32,
}

impl ProgramState {
    fn new() -> ProgramState {
        ProgramState { pc: 0, acc: 0 }
    }
}

fn parse_instruction(s: &str) -> Instruction {
    lazy_static! {
        static ref RE_INSTRUCTION: Regex =
            Regex::new(r"([a-z]+) ([+-]\d+)").expect("failed to compile regex");
    }
    let (opcode, arg) = RE_INSTRUCTION
        .captures(s)
        .map(|caps| {
            (
                // safe to unwrap because the capture groups are not optional
                caps.get(1).unwrap().as_str(),
                caps.get(2)
                    .unwrap()
                    .as_str()
                    .parse::<i32>()
                    .expect("couldn't parse argument"),
            )
        })
        .expect("couldn't parse assembly instruction");
    match opcode {
        "acc" => Instruction::Acc(arg),
        "jmp" => Instruction::Jmp(arg),
        "nop" => Instruction::Nop(arg),
        _ => panic!("unknown opcode"),
    }
}

fn execute_instruction(state: &ProgramState, instr: &Instruction) -> ProgramState {
    match instr {
        Instruction::Acc(incr) => ProgramState {
            pc: state.pc.checked_add(1).expect("overflow"),
            acc: state.acc.checked_add(*incr).expect("overflow"),
        },
        Instruction::Jmp(incr) => ProgramState {
            // arbitrary choice: we only address from 0 to i32::MAX
            pc: incr.checked_add(state.pc as i32).expect("overflow") as u32,
            acc: state.acc,
        },
        Instruction::Nop(_) => ProgramState {
            pc: state.pc.checked_add(1).expect("overflow"),
            acc: state.acc,
        },
    }
}

fn parse_instructions(input: &[&str]) -> Vec<Instruction> {
    input.iter().map(|s| parse_instruction(s)).collect()
}

/// execute instructions until we find we're in a loop, then return the program state
/// pc will be at the first repeated instruction
fn execute_until_loop(imem: &[Instruction]) -> ProgramState {
    let mut state = ProgramState::new();
    let mut pcs = HashSet::new();

    while !pcs.contains(&state.pc) {
        pcs.insert(state.pc);
        let instr = imem
            .get(state.pc as usize)
            .expect("execution went outside program memory!");

        state = execute_instruction(&state, instr);
    }

    state
}

// parse instructions, execute them until we find we're in a loop, and return accumulator value
fn find_acc_at_loop(input: &[&str]) -> i32 {
    execute_until_loop(&parse_instructions(input)).acc
}

// part 2

// Flip the instruction at the given address, nop -> jmp, jmp -> nop
fn flip_execute_instruction(state: &ProgramState, instr: &Instruction) -> ProgramState {
    match instr {
        Instruction::Acc(_) => execute_instruction(state, instr),
        Instruction::Jmp(x) => execute_instruction(state, &Instruction::Nop(*x)),
        Instruction::Nop(x) => execute_instruction(state, &Instruction::Jmp(*x)),
    }
}

// If we execute to the instruction right after the last imem addr, it terminates.
// If the program terminates, get the final accumulator value.
// Flip the instruction at the given address, nop -> jmp, jmp -> nop
fn get_final_state(imem: &[Instruction], flip_addr: u32) -> Option<i32> {
    let mut state = ProgramState::new();
    let mut pcs = HashSet::new();

    while state.pc as usize != imem.len() {
        pcs.insert(state.pc);
        let instr = imem
            .get(state.pc as usize)
            .expect("execution went outside program memory!");

        state = if state.pc == flip_addr {
            flip_execute_instruction(&state, instr)
        } else {
            execute_instruction(&state, instr)
        };

        // loop detection
        if pcs.contains(&state.pc) {
            return None;
        }
    }

    Some(state.acc)
}

// Parse instructions, try flipping each instruction until we find a program that terminates
// Returns the accumulator value of the terminating program, if a terminationg program exists
fn find_acc_at_termination(input: &[&str]) -> Option<i32> {
    let imem = parse_instructions(input);
    (0..imem.len()).find_map(|flip_addr| get_final_state(&imem, flip_addr as u32))
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: [&'static str; 9] = [
        "nop +0", "acc +1", "jmp +4", "acc +3", "jmp -3", "acc -99", "acc +1", "jmp -4", "acc +6",
    ];

    const TEST_IMEM: [Instruction; 9] = [
        Instruction::Nop(0),
        Instruction::Acc(1),
        Instruction::Jmp(4),
        Instruction::Acc(3),
        Instruction::Jmp(-3),
        Instruction::Acc(-99),
        Instruction::Acc(1),
        Instruction::Jmp(-4),
        Instruction::Acc(6),
    ];

    #[test]
    fn test_parse_instruction() {
        assert_eq!(Instruction::Nop(0), parse_instruction(TEST_INPUT[0]));
        assert_eq!(Instruction::Acc(1), parse_instruction(TEST_INPUT[1]));
        assert_eq!(Instruction::Jmp(-3), parse_instruction(TEST_INPUT[4]));
    }

    #[test]
    fn test_parse_instructions() {
        assert_eq!(&parse_instructions(&TEST_INPUT), &TEST_IMEM);
    }

    #[test]
    fn test_execute_instruction() {
        let state0 = ProgramState::new();
        let state1 = ProgramState { pc: 1, acc: 0 };
        let state2 = ProgramState { pc: 2, acc: 1 };
        let state3 = ProgramState { pc: 6, acc: 1 };

        // nop
        assert_eq!(state1, execute_instruction(&state0, &TEST_IMEM[0]));
        // acc
        assert_eq!(state2, execute_instruction(&state1, &TEST_IMEM[1]));
        // jmp
        assert_eq!(state3, execute_instruction(&state2, &TEST_IMEM[2]));
    }

    #[test]
    fn test_find_acc_at_loop() {
        assert_eq!(5, find_acc_at_loop(&TEST_INPUT));
    }

    // part 2
    #[test]
    fn test_get_final_state() {
        for i in 0..7 {
            assert_eq!(None, get_final_state(&TEST_IMEM, i));
        }
        assert_eq!(Some(8), get_final_state(&TEST_IMEM, 7));
    }

    #[test]
    fn test_find_acc_at_termination() {
        assert_eq!(Some(8), find_acc_at_termination(&TEST_INPUT));
    }
}
