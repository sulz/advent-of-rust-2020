// day4.rs
// checking passports

use advent_of_rust_2020::util;
use std::collections::HashSet;

fn main() {
    let input = util::read_all_stdin();
    println!("Part 1: valid passports {}", count_valid_passports(&input));
    println!("Part 2: valid passports {}", count_valid_passports2(&input));
}

fn has_all_fields(passport: &str, required_fields: &HashSet<&str>) -> bool {
    *required_fields
        == passport
            .split_whitespace()
            // "byr:1937" -> "byr"
            .map(|s| s.split(':').next().unwrap_or("invalid"))
            .filter(|key| required_fields.contains(key))
            .collect()
}

fn get_required_fields() -> HashSet<&'static str> {
    ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        .iter()
        .cloned()
        .collect()
}

fn count_valid_passports(input: &str) -> usize {
    let required_fields = get_required_fields();
    input
        .split("\n\n")
        .filter(|passport| has_all_fields(passport, &required_fields))
        .count()
}

// part 2
fn is_valid_field(field: &str) -> bool {
    // I kind of hate this implementation, I should have used parser combinators, or at least regex
    let v: Vec<_> = field.splitn(2, ':').collect();
    if v.len() < 2 {
        // this is not in key:value format so we're not interested in validating
        return true;
    }
    let key = v[0];
    let val = v[1];

    match key {
        "byr" => {
            let year: u32 = val.parse().unwrap_or(0);
            year >= 1920 && year <= 2002
        }
        "iyr" => {
            let year: u32 = val.parse().unwrap_or(0);
            year >= 2010 && year <= 2020
        }
        "eyr" => {
            let year: u32 = val.parse().unwrap_or(0);
            year >= 2020 && year <= 2030
        }
        "hgt" => {
            // a number followed by either cm or in:
            // If cm, the number must be at least 150 and at most 193.
            // If in, the number must be at least 59 and at most 76
            match val.len() {
                5 => {
                    let height: u32 = val
                        .get(0..3)
                        .expect("string too short")
                        .parse()
                        .expect("parse error");
                    height >= 150 && height <= 193 && val.get(3..) == Some("cm")
                }
                4 => {
                    let height: u32 = val
                        .get(0..2)
                        .expect("string too short")
                        .parse()
                        .expect("parse error");
                    height >= 59 && height <= 76 && val.get(2..) == Some("in")
                }
                _ => false,
            }
        }
        "hcl" => {
            // #nnnnnn where nnnnnn is a hexadecimal value
            val.len() == 7
                && val.get(0..1) == Some("#")
                && val.get(1..).unwrap().chars().all(|c| c.is_digit(16))
        }
        "ecl" => {
            val == "amb"
                || val == "blu"
                || val == "brn"
                || val == "gry"
                || val == "grn"
                || val == "hzl"
                || val == "oth"
        }
        "pid" => {
            // 9 decimal digits
            val.len() == 9 && val.chars().all(|c| c.is_digit(10))
        }
        _ => true,
    }
}

fn is_valid_passport(passport: &str) -> bool {
    passport.split_whitespace().all(|f| is_valid_field(f))
}

fn count_valid_passports2(input: &str) -> usize {
    let required_fields = get_required_fields();
    input
        .split("\n\n")
        .filter(|passport| {
            has_all_fields(passport, &required_fields) && is_valid_passport(passport)
        })
        .count()
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_INPUT: &'static str = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";

    #[test]
    fn test_has_all_fields() {
        let required_fields = get_required_fields();
        let passports: Vec<_> = TEST_INPUT.split("\n\n").collect();
        assert_eq!(4, passports.len());
        assert!(has_all_fields(passports[0], &required_fields));
        assert!(!has_all_fields(passports[1], &required_fields));
        assert!(has_all_fields(passports[2], &required_fields));
        assert!(!has_all_fields(passports[3], &required_fields));
    }

    #[test]
    fn test_count_valid_passports() {
        assert_eq!(2, count_valid_passports(TEST_INPUT));
    }

    // part 2
    const TEST_INPUT2: &'static str = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007

pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";

    #[test]
    fn test_is_valid_field() {
        assert!(is_valid_field("byr:2002"));
        assert!(!is_valid_field("byr:2003"));
        assert!(is_valid_field("hgt:60in"));
        assert!(is_valid_field("hgt:190cm"));
        assert!(!is_valid_field("hgt:190in"));
        assert!(!is_valid_field("hgt:190"));
        assert!(is_valid_field("hcl:#123abc"));
        assert!(!is_valid_field("hcl:#123abz"));
        assert!(!is_valid_field("hcl:123abc"));
        assert!(is_valid_field("ecl:brn"));
        assert!(!is_valid_field("ecl:wat"));
        assert!(is_valid_field("pid:000000001"));
        assert!(!is_valid_field("pid:0123456789"));
    }

    #[test]
    fn test_is_valid_passport() {
        let passports: Vec<_> = TEST_INPUT2.split("\n\n").collect();
        assert_eq!(8, passports.len());
        for i in 0..4 {
            assert!(!is_valid_passport(passports[i]));
        }
        for i in 4..8 {
            assert!(is_valid_passport(passports[i]));
        }
    }

    #[test]
    fn test_count_valid_passports2() {
        assert_eq!(4, count_valid_passports2(TEST_INPUT2));
    }
}
