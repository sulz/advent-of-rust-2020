// day25.rs
// public/private key encryption

use advent_of_rust_2020::util;

fn main() {
    let input: Vec<u64> =
        util::parse_all_stdin_lines(|s| s.parse().expect("couldn't parse number"));
    let encryption_key = find_encryption_key(input[1], input[0]);
    println!("part 1 encryption key: {}", encryption_key);
}

const MODULO: u64 = 20201227;

fn find_loop_size(pub_key: u64) -> u64 {
    const SUBJECT_NUMBER: u64 = 7;
    let mut val = 1;
    for loop_size in 1.. {
        val = (val * SUBJECT_NUMBER) % MODULO;
        if val == pub_key {
            return loop_size;
        }
    }
    unreachable!();
}

fn encrypt(pub_key: u64, loop_size: u64) -> u64 {
    (0..loop_size).fold(1, |acc, _| (acc * pub_key) % MODULO)
}

fn find_encryption_key(door_pub_key: u64, card_pub_key: u64) -> u64 {
    let card_loop_size = find_loop_size(card_pub_key);
    encrypt(door_pub_key, card_loop_size)
}

#[cfg(test)]
mod tests {
    use crate::*;

    const TEST_CARD_PUB_KEY: u64 = 5764801;
    const TEST_DOOR_PUB_KEY: u64 = 17807724;

    #[test]
    fn test_find_loop_size() {
        assert_eq!(8, find_loop_size(TEST_CARD_PUB_KEY));
        assert_eq!(11, find_loop_size(TEST_DOOR_PUB_KEY));
    }

    #[test]
    fn test_encrypt() {
        assert_eq!(14897079, encrypt(TEST_DOOR_PUB_KEY, 8));
        assert_eq!(14897079, encrypt(TEST_CARD_PUB_KEY, 11));
    }

    #[test]
    fn test_find_encryption_key() {
        assert_eq!(
            14897079,
            find_encryption_key(TEST_DOOR_PUB_KEY, TEST_CARD_PUB_KEY)
        );
    }
}
