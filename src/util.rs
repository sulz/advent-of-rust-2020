// util.rs
//
// common stuff for advent of code

use std::io::BufRead;
use std::io::Read;

pub fn read_all_stdin() -> String {
    let mut input = String::new();
    std::io::stdin()
        .read_to_string(&mut input)
        .expect("Failed to read input");

    input
}

pub fn read_all_stdin_to_lines() -> Vec<String> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| l.expect("Failed to read line"))
        .collect()
}

pub fn parse_all_stdin_lines<T>(parse_line_fn: impl Fn(&str) -> T) -> Vec<T> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| parse_line_fn(&l.expect("Failed to read line")))
        .collect()
}
